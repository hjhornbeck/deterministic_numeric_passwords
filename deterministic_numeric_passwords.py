#!/usr/bin/env python3

import argparse
from hashlib import shake_256
from math import ceil, log2


def left_encode( x:int ) -> bytes:
    """
    NIST's left-encode() function.
    """
    assert x >= 0
    byte_len = (max(x.bit_length(),1) + 7) >> 3
    return bytes([byte_len]) + x.to_bytes( byte_len, 'big' )


def generate_code_number( seed:bytes, number:int, digits:int=4, base:int=10 ) -> int:
    """
    Generate a number that will become the output code.
    """

    hasher = shake_256( left_encode(len(seed)) + seed + left_encode( number ) )

    # generate the digest we'll convert to a number
    target_bits = digits * log2( base )
    source = hasher.digest( (ceil(target_bits) + 7) >> 3 )
    return int.from_bytes( source, 'big' )


def code_to_digits( number:int, digits:int=4, base:int=10 ) -> list[int]:
    """
    Convert a number into a list of digits for later printing.
    """

    code = list()
    for _ in range( digits ):
        code.append( number % base )
        number //= base

    return code


def digits_to_str( code:list[int], base:int=10 ) -> str:
    """
    Convert a list of numeric digits into a string.
    """
    if base <= 10:
        return ''.join(map(str,code))

    elif base == 16:
        return ''.join([f'{n:x}' for n in code])

    elif base == 256:
        return bytes(code).hex()

    else:
        return repr(code)


if __name__ == '__main__':

    def bounded_int( x, lower:int=None, upper:int=None ):
        val = int(x)
        if (lower is not None) and (val < lower):
            raise ValueError(f"Provided integer is to low, should be greater than or equal to {lower}!")
        if (upper is not None) and (val > upper):
            raise ValueError(f"Provided integer is to high, should be less than or equal to {upper}!")
        return val

    cmdline = argparse.ArgumentParser( description="Generate a deterministic password," + \
            " intended to be used once or in a time-sensitive manner, consisting of a fixed count of numeric digits." )

    cmdline.add_argument( '--seed', '-s', metavar='FILE', type=argparse.FileType('rb', 0), \
        help='The seed file for the cryptographic sequence.' )
    cmdline.add_argument( '--number', '-n', metavar='NUMBER', nargs='+', type=lambda x: bounded_int(x, lower=0), \
        help='The code to return in the sequence, must be positive or zero.' )
    cmdline.add_argument( '--digits', '-d', metavar='NUMBER', default=4, type=lambda x: bounded_int(x, lower=4), \
        help='The number of decimal digits in the code, must be at least four. Lengths below or equal to eight are recommended.' )
    cmdline.add_argument( '--base', '-b', metavar='NUMBER', default=10, type=lambda x: bounded_int(x, lower=2), \
        help='The base to use for each digit. Cannot be less than two.' )

    args = cmdline.parse_args()


    # read in the seed
    seed = args.seed.read()
    args.seed.close()

    # loop over the numbers we were given
    for number in args.number:

        code = generate_code_number( seed, number, args.digits, args.base )
        sequence = code_to_digits( code, args.digits, args.base )
        print( f'{number}\t' + digits_to_str( sequence, args.base ) )

