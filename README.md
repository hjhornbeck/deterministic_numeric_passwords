# Deterministic Numeric Passwords

```
$ python3 deterministic_numeric_passwords.py --help
usage: deterministic_numeric_passwords.py [-h] [--seed FILE] [--number NUMBER [NUMBER ...]] [--digits NUMBER] [--base NUMBER]

Generate a deterministic password, intended to be used once or in a time-sensitive manner, consisting of a fixed count of numeric digits.

options:
  -h, --help            show this help message and exit
  --seed FILE, -s FILE  The seed file for the cryptographic sequence.
  --number NUMBER [NUMBER ...], -n NUMBER [NUMBER ...]
                        The code to return in the sequence, must be positive or zero.
  --digits NUMBER, -d NUMBER
                        The number of decimal digits in the code, must be at least four. Lengths below or equal to eight are recommended.
  --base NUMBER, -b NUMBER
                        The base to use for each digit. Cannot be less than two.

$ dd if=/dev/urandom of=seed.bin bs=32 count=1    # you can substitute any file you wish, but if you have none this approach is recommended
1+0 records in
1+0 records out
32 bytes copied, 5.8598e-05 s, 546 kB/s

$ python3 deterministic_numeric_passwords.py --seed seed.bin -n {0..4} -d 6 -b 16
0       8e11d4
1       4b7bbf
2       524418
3       f1b6e1
4       52980a
                                                                                                     
$ python3 deterministic_numeric_passwords.py --seed seed.bin --number 0 --digits 4 --base 256
0       35e8114d

$ # note the resemblance to entry zero above, the endianness has just changed
```
